# IPTrack #

ASP.NET MVC Angular App for Tracking/Managing IP Addresses per Client.

### To set up SQL Server database ###

* Run the following in Package manager console of `IPTrack` project
```
add-migration initial
update-database
```
* Run the app. The website automatically added `admin` user with the password `123456789`