﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack
{
    public class App
    {
        public static DatabaseContext Database
        {
            get
            {
                return new DatabaseContext();
            }
        }

        public static void Initialize()
        {
            var db = App.Database;

            // create admin if does not exist
            var admin = db.Users.FirstOrDefault(u => u.username == "admin");
            if (admin == null)
            {
                var newAdmin = new UserTable()
                {
                    username = "admin",
                    password = "123456789",
                    roles = "admin"
                };
                newAdmin.HashPassword(20);
                db.Users.Add(newAdmin);
                db.SaveChanges();
            }

            // do something else
        }
    }
}
