﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Models
{
    public class Logs
    {



        public static void Log(string username, string desciption)
        {
            var db = App.Database;
            db.Logs.Add(new LogTable()
            {
                userId = new User(username: username).profile.id,
                description = desciption,
                timestamp = DateTime.Now
            });
            db.SaveChanges();
        }



    }
}
