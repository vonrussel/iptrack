﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Models
{
    public class DeviceType
    {
        int id;
        public DeviceTypeTable profile;

        public DeviceType(DeviceTypeTable profile) {
            this.profile = profile;
        }

        public DeviceType(int id)
        {
            this.id = id;
            this.profile = this.GetProfile();
        }



        public DeviceTypeTable GetProfile()
        {
            var db = App.Database;
            return db.DeviceTypes.FirstOrDefault(u => 
                (this.id > 0 && u.id == this.id)
            );
        }

        public void Save(string username = "")
        {
            var db = App.Database;

            if (this.profile != null && id <= 0)
            {
                // TODO: add security
                db.DeviceTypes.Add(this.profile);
                db.SaveChanges();

                Logs.Log(username,
                    String.Format("New Device type [{0}] has been added",
                        this.profile.name + " [" + this.profile.id + "]"
                ));
            }
            else
            {
                // update
                var type = db.DeviceTypes.FirstOrDefault(c => c.id == this.id);
                if (type != null)
                {
                    type.name = profile.name;
                    db.SaveChanges();

                    Logs.Log(username,
                        String.Format("Device type [{0}] has been updated",
                            type.name + " [" + type.id + "]"
                    ));
                }
            }
        }





        #region Statics
        public static IEnumerable<DeviceTypeTable> Get(Expression<Func<DeviceTypeTable, bool>> expression)
        {
            return App.Database.DeviceTypes.Where(expression);
        }

        public static IEnumerable<DeviceType> GetAll()
        {
            var db = App.Database;
            return (IEnumerable<DeviceType>)db.DeviceTypes
                .Where(c => c.id != null)
                .ToList()
                .Select(c => new DeviceType(c.id));
        }
        #endregion

    }
}
