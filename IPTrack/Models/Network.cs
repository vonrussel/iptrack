﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Models
{
    public class Network
    {
        int id;
        public NetworkTable profile;

        public Network(NetworkTable profile) {
            if(profile.Client != null)
                profile.Client.ArchivedByUser = null;
            if(profile.Location != null)
                profile.Location.Client = null;
            this.profile = profile;
        }

        public Network(int id)
        {
            this.id = id;
            this.profile = this.GetProfile();
        }



        public NetworkTable GetProfile()
        {
            var db = App.Database;
            var network = db.Networks.FirstOrDefault(u => 
                (this.id > 0 && u.id == this.id)
            );
            network.Client.ArchivedByUser = null;
            network.Location.Client = null;
            return network;
        }

        public void Save(string username = "")
        {
            var db = App.Database;

            if (this.profile != null && id <= 0)
            {
                // TODO: add security
                db.Networks.Add(this.profile);
                db.SaveChanges();

                Logs.Log(username,
                    String.Format("Network [{0}] has been added",
                        this.profile.name + " [" + this.profile.id + "]"
                ));
            }
            else
            {
                // update
                var network = db.Networks.FirstOrDefault(c => c.id == this.id);
                if (network != null)
                {
                    network.name = profile.name;
                    network.cClass = profile.cClass;
                    network.networkTypeId = profile.networkTypeId;
                    network.locationId = profile.locationId;
                    network.clientId = profile.clientId;
                    network.rangeFrom = profile.rangeFrom;
                    network.rangeTo = profile.rangeTo;
                    network.dhcpFrom = profile.dhcpFrom;
                    network.DHCPServer = profile.DHCPServer;
                    network.dhcpTo = profile.dhcpTo;
                    network.subnet = profile.subnet;
                    network.comments = profile.comments;
                    network.dns1 = profile.dns1;
                    network.dns2 = profile.dns2;
                    network.dns3 = profile.dns3;
                    network.dns4 = profile.dns4;
                    db.SaveChanges();

                    Logs.Log(username,
                       String.Format("Network [{0}] has been updated",
                           network.name + " [" + network.id + "]"
                   ));
                }
            }
        }





        #region Statics
        public static IEnumerable<Network> GetAll()
        {
            var db = App.Database;
            return (IEnumerable<Network>)db.Networks
                .Where(c => c.id != null)
                .ToList()
                .Select(c => {
                    var network = new Network(c.id);
                    network.profile.ArchivedByUser = null;
                    network.profile.Client.ArchivedByUser = null;
                    network.profile.Location.Client = null;
                    return network;
                });
        }
        #endregion

    }
}
