﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Models
{
    public class Location
    {
        int id;
        public LocationTable profile;

        public Location(LocationTable profile) {
            if(profile.Client != null)
                profile.Client.ArchivedByUser = null;
            this.profile = profile;
        }

        public Location(int id)
        {
            this.id = id;
            this.profile = this.GetProfile();
        }



        public LocationTable GetProfile()
        {
            var db = App.Database;
            var location = db.Locations.FirstOrDefault(u => 
                (this.id > 0 && u.id == this.id)
            );
            location.Client.ArchivedByUser = null;
            return location;
        }

        public void Save(string username = "")
        {
            var db = App.Database;

            if (this.profile != null && id <= 0)
            {
                // TODO: add security
                db.Locations.Add(this.profile);
                db.SaveChanges();

                Logs.Log(username,
                    String.Format("New Location [{0}] has been added",
                        this.profile.name + " [" + this.profile.id + "]"
                ));
            }
            else
            {
                // update
                var client = db.Locations.FirstOrDefault(c => c.id == this.id);
                if (client != null)
                {
                    client.name = profile.name;
                    client.address = profile.address;
                    client.locationPh1 = profile.locationPh1;
                    client.locationPh2 = profile.locationPh2;
                    client.contactName = profile.contactName;
                    db.SaveChanges();

                    Logs.Log(username,
                        String.Format("Location [{0}] has been updated",
                            client.name + " [" + client.id + "]"
                    ));
                }
            }
        }





        #region Statics
        public static IEnumerable<Location> GetAll()
        {
            var db = App.Database;
            return (IEnumerable<Location>)db.Locations
                .Where(c => c.id != null)
                .ToList()
                .Select(c => new Location(c.id));
        }
        #endregion

    }
}
