﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack
{
    public class Reports
    {

        public static string ExportIPAddressToCSV(int networkId, string path, string fileName = "")
        {
            var network = new IPTrack.Models.Network(networkId);
            var assigned = IPTrack.Models.IPTracker.GetAllAssigned().Where(n => n.networkId == networkId);
            var toExport = new List<IPAddressesExportModel>();

            for (int i = network.profile.rangeFrom; i <= network.profile.rangeTo; i++)
            {
                int dhcpFrom = network.profile.dhcpFrom;
                int dhcpTo = network.profile.dhcpTo;
                var assignedItem =  assigned.FirstOrDefault(a => a.localAddress == i.ToString());

                toExport.Add(new IPAddressesExportModel()
                {
                    IPAddress = network.profile.cClass + "." + i.ToString(),
                    isDHCP = i >= dhcpFrom && i <= dhcpTo ? "Yes" : "",
                    used = assignedItem != null ? "Yes" : "",
                    information = assignedItem != null ? assignedItem.information : "",
                    comment = assignedItem != null ? assignedItem.comment : "",
                    deviceName = assignedItem != null ? assignedItem.DeviceType.name : ""
                });
            }

            var csv = new xx.Excel.CsvExport<IPAddressesExportModel>(toExport);
            if (String.IsNullOrEmpty(fileName))
            {
                fileName = xx.String_.GenerateRandom(10) + ".csv";
            }

            csv.ExportToFile(path + fileName, true);
            return fileName;
        }
          
        public class IPAddressesExportModel
        {
            public string IPAddress { get; set; }
            public string isDHCP { get; set; }
            public string used { get; set; }

            public string deviceName { get; set; }
            public string information { get; set; }
            public string comment { get; set; }
        }

    }
}
