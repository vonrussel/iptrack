﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Models
{
    public class User
    {
        int id;
        string username;
        string email;

        public UserTable profile;


        public User(UserTable profile)
        {
            this.profile = profile;
        }

        public User(int id = 0, string username = "", string email = "")
        {
            this.id = id;
            this.username = username;
            this.email = email;
            this.profile = this.GetProfile();
        }

        public UserTable GetProfile()
        {
            var db = App.Database;
            var user = db.Users.FirstOrDefault(u => 
                (this.id > 0 && u.id == this.id) ||
                (this.username != "" && u.username == this.username) ||
                (this.email != "" && u.email == this.email)
            );

            return user;
        }

        public static bool Auth(string usernameOrEmail, string password)
        {
            var db = App.Database;
            var user = db.Users.FirstOrDefault(u => u.username == usernameOrEmail || u.email == usernameOrEmail);
            if (user != null && !user.archived)
            {
                return user.password == xx.Security.PasswordHasher.Hash(password, user.password_salt);
            }
            return false;
        }




        public void Save(string username = "")
        {
            var db = App.Database;

            if (this.profile != null && id <= 0)
            {
                // TODO: add security
                this.profile.HashPassword(20);
                db.Users.Add(this.profile);
                db.SaveChanges();

                Logs.Log(username,
                    String.Format("New user [{0}] has been added",
                        this.profile.username + " [" + this.profile.id + "]"
                ));
            }
            else
            {
                // update
                var user = db.Users.FirstOrDefault(c => c.id == this.id);
                if (user != null)
                {
                    user.username = profile.username;
                    user.roles = profile.roles;
                    user.email = profile.email;
                    db.SaveChanges();

                    Logs.Log(username,
                        String.Format("User [{0}] has been updated",
                            user.username + " [" + user.id + "]"
                    ));
                }
            }
        }

        // must have id
        public void Archive(User currentUser)
        {
            var db = App.Database;
            var user = db.Users.FirstOrDefault(u => u.id == this.id);
            if (user != null)
            {
                user.archived = true;
                user.archivedBy = currentUser.profile.id;
                user.archivedWhen = DateTime.Now;
                db.SaveChanges();
            }
        }

        #region Statics
        public static IEnumerable<User> GetAll()
        {
            var db = App.Database;
            return (IEnumerable<User>)db.Users
                .Where(c => !c.archived)
                .ToList()
                .Select(c => new User(c.id));
        }
        #endregion




    }
}
