﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Models
{
    public class Client
    {
        int id;
        public ClientTable profile;

        public Client(ClientTable profile) {
            // we hide sensitive data
            if (profile.ArchivedByUser != null)
            {
                profile.ArchivedByUser.password = null;
                profile.ArchivedByUser.password_salt = null;
            }
            this.profile = profile;
        }

        public Client(int id)
        {
            this.id = id;
            this.profile = this.GetProfile();
        }

        public ClientTable GetProfile()
        {
            var db = App.Database;
            var client = db.Clients.FirstOrDefault(u => 
                (this.id > 0 && u.id == this.id)
            );
            // we hide sensitive data
            if (client.ArchivedByUser != null)
            {
                client.ArchivedByUser.password = null;
                client.ArchivedByUser.password_salt = null;
            }

            return client;
        }

        public void Save(string username = "")
        {
            var db = App.Database;

            if (this.profile != null && id <= 0)
            {
                // TODO: add security
                db.Clients.Add(this.profile);
                db.SaveChanges();

                Logs.Log(username,
                    String.Format("New client [{0}] has been added",
                        this.profile.name + " [" + this.profile.id + "]"
                ));
            }
            else
            {
                // update
                var client = db.Clients.FirstOrDefault(c => c.id == this.id);
                if (client != null)
                {
                    client.name = profile.name;
                    db.SaveChanges();

                    Logs.Log(username,
                        String.Format("Client [{0}] has been updated",
                            client.name + " [" + client.id + "]"
                    ));
                }
            }
        }





        #region Statics
        public static IEnumerable<ClientTable> Get(Expression<Func<ClientTable, bool>> expression) {
            return App.Database.Clients.Where(expression);
        }

        public static IEnumerable<Client> GetAll(bool includeDeleted = false)
        {
            var db = App.Database;
            var clients = (IEnumerable<Client>)db.Clients
                .Where(c => c.id != null)
                .ToList()
                .Select(c => new Client(c.id));

            if(!includeDeleted) {
                clients = clients.Where(c => !c.profile.archived);
            }

            // we hide sensitive data
            clients = clients.Select(c =>
            {
                if(c.profile.ArchivedByUser != null) {
                    c.profile.ArchivedByUser.password = null;
                    c.profile.ArchivedByUser.password_salt = null;
                }
                return c;
            });
            return clients;
        }

        public static void Delete(int id, User user)
        {
            var db = App.Database;
            var client = db.Clients.FirstOrDefault(c => c.id == id);
            if (client != null)
            {
                var userId = user.GetProfile().id;
                client.archived = true;
                client.ArchivedByUser = db.Users.FirstOrDefault(u => u.id == userId);
                client.archivedDate = DateTime.Now;
                db.SaveChanges();
            }
        }
        #endregion

    }
}
