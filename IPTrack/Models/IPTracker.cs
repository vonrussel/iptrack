﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Models
{
    public class IPTracker
    {


        public static void AssignToIp(string ipAddress, int deviceType, int networkId, string comment = "", string information = "", User currentUser = null) {
            var db = App.Database;
            string localAddress = "";

            if (ipAddress == null || networkId == null || currentUser == null)
            {
                throw new Exception("Invalid parameters");
            }

            // get network local address
            var network = db.Networks.FirstOrDefault(n => n.id == networkId);
            if (network != null) {
                // remove c class part
                localAddress = ipAddress.Replace(network.cClass + ".", "");
            }

            var ip = new IPAddressTable()
            {
                IpAddress = ipAddress,
                localAddress = localAddress,
                networkId = networkId,
                information = information,
                comment = comment,
                deviceTypeId = deviceType,
                used = true,
                createdBy = currentUser.profile.id,
                createdWhen = DateTime.Now
            };

            db.IPAddresses.Add(ip);
            db.SaveChanges();

            string ipNetwork = ip.Network.name + "[" + networkId + "]";
            Logs.Log(currentUser.profile.username,
                String.Format("IP address [{0}] asssigned to network [{1}] with ID of [{2}]",
                    ipAddress,
                    ipNetwork,
                    ip.id
            ));
        }


        public static void Update(int id, IPAddressTable data, User currentUser)
        {
            var db = App.Database;
            var ip = db.IPAddresses.FirstOrDefault(i => i.id == id);

            if (ip == null || data == null || currentUser == null)
            {
                throw new Exception("Invalid parameters");
            }

            ip.information = data.information;
            ip.comment = data.comment;
            ip.lastEditedBy = currentUser.GetProfile().id;
            ip.lastEditedWhen = DateTime.Now;
            ip.used = true;
            db.SaveChanges();

            string ipNetwork = ip.Network.name + "[" + ip.networkId + "]";
            Logs.Log(currentUser.profile.username,
                String.Format("IP address [{0}] from network [{1}] with ID of [{2}] has been updated",
                    ip.IpAddress,
                    ipNetwork,
                    ip.id
            ));
        }

        public static void DeleteAssigned(int id, User currentUser)
        {
            var db = App.Database;
            var ip = db.IPAddresses.FirstOrDefault(i => i.id == id);

            if (ip == null || currentUser == null)
            {
                throw new Exception("Invalid parameters");
            }

            ip.lastEditedBy = currentUser.GetProfile().id;
            ip.lastEditedWhen = DateTime.Now;
            ip.used = false;
            db.SaveChanges();

            string ipNetwork = ip.Network.name + "[" + ip.networkId + "]";
            Logs.Log(currentUser.profile.username,
                String.Format("IP address [{0}] from network [{1}] with ID of [{2}] assignment has been removed",
                    ip.IpAddress,
                    ipNetwork,
                    ip.id
            ));
        }


        public static IEnumerable<IPAddressTable> GetAllAssigned()
        {
            var db = App.Database;
            return db.IPAddresses.Where(ip => ip.id != null).ToList()
                .Select(ip =>
                {
                    ip.LastEditedByUser = null;
                    ip.CreatedByUser = null;
                    return ip;
                });
        }

        public static IPAddressTable GetIP(int ipId)
        {
            var db = App.Database;
            var _ip = db.IPAddresses.FirstOrDefault(ip => ip.id == ipId);
            _ip.LastEditedByUser = null;
            _ip.CreatedByUser = null;
            return _ip;
        }

        public static IPAddressTable GetByLocal(int local)
        {
            var db = App.Database;
            var _ip = db.IPAddresses.FirstOrDefault(ip => ip.localAddress == local.ToString());
            _ip.LastEditedByUser = null;
            _ip.CreatedByUser = null;
            return _ip;
        }


    }
}
