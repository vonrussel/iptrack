﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Models
{
    public class NetworkType
    {
        int id;
        public NetworkTypeTable profile;

        public NetworkType(NetworkTypeTable profile) {
            this.profile = profile;
        }

        public NetworkType(int id)
        {
            this.id = id;
            this.profile = this.GetProfile();
        }



        public NetworkTypeTable GetProfile()
        {
            var db = App.Database;
            return db.NetworkTypes.FirstOrDefault(u => 
                (this.id > 0 && u.id == this.id)
            );
        }

        public void Save(string username = "")
        {
            var db = App.Database;

            if (this.profile != null && id <= 0)
            {
                // TODO: add security
                db.NetworkTypes.Add(this.profile);
                db.SaveChanges();

                Logs.Log(username,
                    String.Format("New Network type [{0}] has been added",
                        this.profile.name + " [" + this.profile.id + "]"
                ));
            }
            else
            {
                // update
                var type = db.NetworkTypes.FirstOrDefault(c => c.id == this.id);
                if (type != null)
                {
                    type.name = profile.name;
                    db.SaveChanges();

                    Logs.Log(username,
                        String.Format("Network type [{0}] has been updated",
                            type.name + " [" + type.id + "]"
                    ));
                }
            }
        }





        #region Statics
        public static IEnumerable<NetworkTypeTable> Get(Expression<Func<NetworkTypeTable, bool>> expression)
        {
            return App.Database.NetworkTypes.Where(expression);
        }

        public static IEnumerable<NetworkType> GetAll()
        {
            var db = App.Database;
            return (IEnumerable<NetworkType>)db.NetworkTypes
                .Where(c => c.id != null)
                .ToList()
                .Select(c => new NetworkType(c.id));
        }
        #endregion

    }
}
