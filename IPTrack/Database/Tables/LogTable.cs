﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPTrack
{
    [Table("Logs")]
    public class LogTable
    {
        public int id { get; set; }
        public string description { get; set; }

        public int? userId { get; set; }
        [ForeignKey("userId")]
        public virtual UserTable User { get; set; }

        public DateTime? timestamp { get; set; }
    }
}
