﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPTrack
{
    [Table("Clients")]
    public class ClientTable
    {
        public int id { get; set; }
        public string name { get; set; }

        public bool archived { get; set; }
        public int? archivedBy { get; set; }
        [ForeignKey("archivedBy")]
        public virtual UserTable ArchivedByUser { get; set; }

        public DateTime? archivedDate { get; set; }

        public static ClientTable FromDynamic(dynamic dyn)
        {
            return new ClientTable()
            {
                name = (String)dyn.name
            };
        }

    }
}
