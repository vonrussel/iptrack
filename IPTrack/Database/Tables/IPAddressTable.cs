﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPTrack
{

    [Table("IPAddresses")]
    public class IPAddressTable
    {
        public int id { get; set; }

        public string IpAddress { get; set; }

        public string localAddress { get; set; }

        public int networkId { get; set; }
        [ForeignKey("networkId")]
        public virtual NetworkTable Network { get; set; }

        public bool used { get; set; }
        public int deviceTypeId { get; set; }
        [ForeignKey("deviceTypeId")]
        public virtual DeviceTypeTable DeviceType { get; set; }
        public bool dhcp { get; set; }

        public int? createdBy { get; set; }
        [ForeignKey("createdBy")]
        public virtual UserTable CreatedByUser { get; set; }

        public DateTime createdWhen { get; set; }

        public int? lastEditedBy { get; set; }
        [ForeignKey("lastEditedBy")]
        public virtual UserTable LastEditedByUser { get; set; }

        public DateTime? lastEditedWhen { get; set; }
        public string information { get; set; }
        public string comment { get; set; }


        public static IPAddressTable FromDynamic(dynamic dyn)
        {
            return new IPAddressTable()
            {
                IpAddress = (String)dyn.IpAddress,
                information = (String)dyn.information,
                comment = (String)dyn.comment,
                deviceTypeId = (int)dyn.deviceTypeId,
                networkId = (int)dyn.networkId,
                used = (bool)dyn.used,
                dhcp = (bool)dyn.dhcp,
                localAddress = (String)dyn.localAddress
            };
        }

    }
}
