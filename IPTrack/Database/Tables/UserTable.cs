﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPTrack
{

    [Table("Users")]
    public class UserTable : xx.BaseUser
    {
        [Key]
        public int id { get; set; }

        // separated with ;
        public string roles { get; set; }

        public string email { get; set; }

        public bool archived { get; set; }
        public DateTime? archivedWhen { get; set; }
        
        
        public int? archivedBy { get; set; }
        [ForeignKey("archivedBy")]
        public virtual UserTable archivedByUser { get; set; }


        public static UserTable FromDynamic(dynamic dyn)
        {
            return new UserTable()
            {
                roles = (String)dyn.roles,
                username = (String)dyn.username,
                password = (String)dyn.password,
                email = (String)dyn.email
            };
        }

    }
}
