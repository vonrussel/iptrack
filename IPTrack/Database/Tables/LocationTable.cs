﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPTrack
{

    [Table("Locations")]
    public class LocationTable
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string locationPh1 { get; set; }
        public string locationPh2 { get; set; }
        public string contactName { get; set; }


        public int? clientId { get; set; }
        [ForeignKey("clientId")]
        public virtual ClientTable Client { get; set; }

        
    }
}
