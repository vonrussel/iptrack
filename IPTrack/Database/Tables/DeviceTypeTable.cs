﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPTrack
{

    [Table("DeviceTypes")]
    public class DeviceTypeTable
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }

        public static DeviceTypeTable FromDynamic(dynamic dyn)
        {
            return new DeviceTypeTable()
            {
                name = (String)dyn.name
            };
        }

    }
}
