﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPTrack
{

    [Table("Networks")]
    public class NetworkTable
    {
        [Key]
        public int id { get; set; }

        public string name { get; set; }

        public int locationId { get; set; }
        [ForeignKey("locationId")]
        public virtual LocationTable Location { get; set; }

        public int clientId { get; set; }
        [ForeignKey("clientId")]
        public virtual ClientTable Client { get; set; }

        public int networkTypeId { get; set; }
        [ForeignKey("networkTypeId")]
        public virtual NetworkTypeTable NetworkType { get; set; }

        public int rangeFrom { get; set; }
        public int rangeTo { get; set; }
        public string DHCPServer { get; set; }

        public int dhcpFrom { get; set; }
        public int dhcpTo { get; set; }
        public string subnet { get; set; }
        public string gateway { get; set; }
        public string comments { get; set; }
        public string cClass { get; set; }
        public string dns1 { get; set; }
        public string dns2 { get; set; }
        public string dns3 { get; set; }
        public string dns4 { get; set; }

        public int? archivedBy { get; set; }
        [ForeignKey("archivedBy")]
        public virtual UserTable ArchivedByUser { get; set; }

        public DateTime? archivedDate { get; set; }



        public static NetworkTable FromDynamic(dynamic dyn)
        {
            return new NetworkTable() {
                name = (String)dyn.name,
                cClass = (String)dyn.cClass,
                networkTypeId = (int)dyn.networkTypeId,
                clientId = (int)dyn.clientId,
                locationId = (int)dyn.locationId,
                rangeFrom = (int)dyn.rangeFrom,
                rangeTo = (int)dyn.rangeTo,
                dhcpFrom = (int)dyn.dhcpFrom,
                dhcpTo = (int)dyn.dhcpTo,
                DHCPServer = (String)dyn.DHCPServer,
                subnet = (String)dyn.subnet,
                comments = (String)dyn.comments,
                dns1 = (String)dyn.dns1,
                dns2 = (String)dyn.dns2,
                dns3 = (String)dyn.dns3,
                dns4 = (String)dyn.dns4
            };
        }

    }
}
