﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPTrack
{

    [Table("NetworkTypes")]
    public class NetworkTypeTable
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }

        public static NetworkTypeTable FromDynamic(dynamic dyn)
        {
            return new NetworkTypeTable()
            {
                name = (String)dyn.name
            };
        }

    }
}
