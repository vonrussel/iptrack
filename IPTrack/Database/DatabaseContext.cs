﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace IPTrack
{


    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base("name=Default")
        { }


        public DbSet<UserTable> Users { get; set; }
        public DbSet<ClientTable> Clients { get; set; }
        public DbSet<LocationTable> Locations { get; set; }
        public DbSet<IPAddressTable> IPAddresses { get; set; }
        public DbSet<NetworkTable> Networks { get; set; }
        public DbSet<NetworkTypeTable> NetworkTypes { get; set; }
        public DbSet<DeviceTypeTable> DeviceTypes { get; set; }
        public DbSet<LogTable> Logs { get; set; }







        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            /*
            // RosterRequests table
            modelBuilder.Entity<TeacherProfileTable>()
                .HasMany(c => c.RosterRequests)
                .WithRequired()
                .HasForeignKey(c => c.teacher_id);

            modelBuilder.Entity<SchoolProfileTable>()
                .HasMany(c => c.RosterRequests)
                .WithRequired()
                .HasForeignKey(c => c.school_id);

            // SchoolTeacherRequests table
            modelBuilder.Entity<TeacherProfileTable>()
                .HasMany(c => c.SchoolTeacherRequests)
                .WithRequired()
                .HasForeignKey(c => c.teacher_id);

            modelBuilder.Entity<SchoolProfileTable>()
                .HasMany(c => c.SchoolTeacherRequests)
                .WithRequired()
                .HasForeignKey(c => c.school_id);



            // coordinates
            modelBuilder.Entity<LocationTable>().Property(l => l.lat).HasPrecision(8, 5);
            modelBuilder.Entity<LocationTable>().Property(l => l.lng).HasPrecision(8, 5);
            */

        }

    }
}
