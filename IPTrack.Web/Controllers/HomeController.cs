﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IPTrack.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // data params
            var authenticated = User.Identity.IsAuthenticated;
            var user = authenticated ? new IPTrack.Models.User(username: User.Identity.Name) : null;

            if (user != null)
            {
                // remove password fields
                user.profile.password = null;
                user.profile.password_salt = null;
            }

            var json = Json(new
            {
                authenticated = authenticated,
                currentUser = user,
                currentDate = new DateTime()
            });
            ViewBag.siteData = new JavaScriptSerializer().Serialize(json.Data);


            return View();
        }
    }
}