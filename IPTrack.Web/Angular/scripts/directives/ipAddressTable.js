app.directive('ipAddressTable', ['$modal', 'networkService', '$http', 'iptrackerService', '$window',
    function($modal, networkService, $http, iptrackerService, $window) {


  return {
    restrict: 'A',
    scope: {
      clientId: '=clientId',
      locationId: '=locationId',
      network: '=network'
    },
    link: function(scope, element, attrs) {

      var defaults = {
        cClass: '1.1.1'
      };
      attrs = angular.extend(defaults, attrs);

      // get ranges from network
      scope.$watch('network', function(network) {
        refreshIPAddressTable(network)
      });

      if(! _.isEmpty($window.previousModal)) {
        // if we have an previous modal, show the modal again
        // this happens when user clicks other data manager in the popup modal

        modal = $modal.open($window.previousModal.state);
        modal.result.then(function() {
          $window.previousModal = {}; // clear previous state of modal
          refreshIPAddressTable(scope.network)
        })
      }

      scope.popup = function(controller, hostId, ipId) {
        // will be used to cache modal state
        // when redirected back
        $window.previousModal = {
          dataState: {},
          state: {}
        }
        $window.previousModal.state = {
          //scope: scope,
          controller: controller,
          size: 'lg',
          resolve: {
            ipId: function() {
              return ipId || null;
            },
            hostId: function() {
              return hostId || null;
            },
            clientId: function() {
              return scope.clientId
            },
            locationId: function() {
              return scope.locationId
            },
            networkId: function() {
              return scope.network.id
            },
            network: function() {
              return scope.network;
            }
          },
          templateUrl: '/angular/views/ip-address/' + (controller == 'editIpController' ? 'edit.html' : 'assign.html')
        };
        
        modal = $modal.open($window.previousModal.state);
        modal.result.then(function() {
          $window.previousModal = null; // clear previous state of modal
          refreshIPAddressTable(scope.network)
        })
      };

      scope.refreshTable = function() {
        refreshIPAddressTable(scope.network)
      };

      function refreshIPAddressTable(network) {
        scope.ipAddresses = [];
        if(!network) return;

        // get all assiged first
        iptrackerService.getAll(network.id)
          .then(function(ipAddresses) {
            for(var i = scope.network.rangeFrom; i <= scope.network.rangeTo; i++) {
              isDHCP = i >= scope.network.dhcpFrom && i <= scope.network.dhcpTo;
              data = _.find(ipAddresses, function(ip) {
                return ip.localAddress == i && ip.used
              });
              scope.ipAddresses.push({
                hostId: i,
                ipAddress: scope.network.cClass + '.' + i,
                data: data,
                isDHCP: isDHCP,
                // hide dhcp ips but display starting/ending IP & assigned statics
                dhcpHidden: isDHCP && (!(data && data.used) && i != scope.network.dhcpFrom && i != scope.network.dhcpTo),
                dhcpStart: i == scope.network.dhcpFrom,
                dhcpEnd: i == scope.network.dhcpTo
              });
            }
          })
      }

      scope.$parent.$ipTable = scope;

    },
    templateUrl: '/angular/views/ip-address/table.html'
  }


}])



.controller('assignIpController', 
    ['$scope', 'hostId', 'clientId', 'locationId', 'network', '$modalInstance', '$http', 'iptrackerService', 'deviceTypeService', '$window',
      function($scope, hostId, clientId, locationId, network, $modalInstance, $http, iptrackerService, deviceTypeService, $window) {

  var ipAddress = network.cClass + "." + hostId;

  angular.extend($scope, {
    // inits
    toAssign: ($window.previousModal && $window.previousModal.dataState) || {},

    // locals
    chooseIp: !hostId,
    IpAddress: ipAddress,
    clientId: clientId,
    locationId: locationId,
    network: network,
    hostId: hostId,
    availableDHCPs: [],

    // select2
    deviceTypes: [],
    deviceTypesLoading: false,
    getDeviceTypes: function(name) {
      $scope.deviceTypesLoading = true;
      deviceTypeService.getAll()
        .then(function(response) {
          $scope.deviceTypesLoading = false;
          $scope.deviceTypes = response
        });
    },

    // methods
    cancel: function() {
      $modalInstance.dismiss('cancel');
    },
    onChange: function(toAssign) {
      if($window.previousModal) {
        $window.previousModal.dataState = toAssign
      }
    },
    assign: function(toAssign) {
      toAssign = toAssign || {};
      toAssign.hostId = hostId;
      toAssign.networkId = network.id;
      toAssign.IpAddress = toAssign.IpAddress || ipAddress;

      iptrackerService.assign(toAssign)
        .then(function() {
          $modalInstance.close();
        })
    }
  });

  iptrackerService.getAll(network.id)
    .then(function(response) {
        for(var i = network.dhcpFrom; i <= network.dhcpTo; i++) {
          // if current ip in the loop isnt assigned or used
          if(!_.find(response, function(ip) { return ip.localAddress == i && ip.used } )) {
            $scope.availableDHCPs.push(network.cClass + '.' + i)
          }
        }
    });

}])






.controller('editIpController', 
    ['$scope', 'hostId', 'clientId', 'locationId', 'network', '$modalInstance', '$http', 'iptrackerService', 'deviceTypeService', 'ipId', '$window',
      function($scope, hostId, clientId, locationId, network, $modalInstance, $http, iptrackerService, deviceTypeService, ipId, $window) {

  var ipAddress = network.cClass + "." + hostId;

  angular.extend($scope, {
    // inits
    model: {},

    // locals
    IpAddress: ipAddress,
    clientId: clientId,
    locationId: locationId,
    network: network,
    hostId: hostId,

    deviceTypes: [],
    deviceTypesLoading: false,
    getDeviceTypes: function(name) {
      $scope.deviceTypesLoading = true;
      deviceTypeService.getAll()
        .then(function(response) {
          $scope.deviceTypesLoading = false;
          $scope.deviceTypes = response
        });
    },

    // methods
    cancel: function() {
      $modalInstance.dismiss();
    },
    onChange: function(toAssign) {
      if($window.previousModal) {
        $window.previousModal.dataState = toAssign
      }
    },
    update: function(model) {
      model = model || {};
      if(! model) return;

      iptrackerService.update(ipId, model)
        .then(function() {
          $modalInstance.close();
        })
    },
    deleteAssigned: function() {
      if(! confirm("Confirm delete of assigned IP?")) return;
      iptrackerService.deleteAssigned(ipId)
        .then(function() {
          $modalInstance.close();
        })
    }
  })

  // get ip data
  if($window.previousModal && !_.isEmpty($window.previousModal.dataState)) {
    $scope.model = $window.previousModal.dataState;
  } else {
    iptrackerService.get(ipId)
      .then(function(data) {
        $scope.model = data;
      });
  }


}]);