Services.service('networkService', function($http, $q) {
  var apiUrl = '/api/networks';

  function appendNameCClass(arr) {
    // add network name + cClass
    return _.map(arr, function(network) {
      return _.extend(network, {nameCClass: network.profile.name + ' (' + network.profile.cClass + ')'})
    })
  }

  return {
    getAll: function() {
      return $q(function(resolve) {
        $http.get(apiUrl)
        .then(function(response) {
          response.data = appendNameCClass(response.data);
          resolve(response.data)
        });
      });
    },

    getClientOf: function(clientId) {
      return $q(function(resolve) {
        url = apiUrl + '?clientId=' + clientId;
        $http.get(url)
        .then(function(response) {
          response.data = appendNameCClass(response.data);
          resolve(response.data)
        });
      });
    },

    getByLocation: function(locationId, params) {
      return $q(function(resolve) {
        params = params || {};
        url = apiUrl + '/GetByLocation/' + locationId;
        $http({
          url: url,
          method: 'GET',
          params: {
            term: params.term
          }
        })
          .then(function(response) {
            response.data = appendNameCClass(response.data);
            console.log(response.data)
            resolve(response.data)
          });
      });
    },

    get: function(id) {
      return $q(function(resolve) {
        $http.get(apiUrl + '/' + id)
        .then(function(response) {
          resolve(response.data)
        });
      });
    },

    insert: function(data) {
      return $q(function(resolve, reject) {
        $http.post(apiUrl + '/insert', data)
        .success(function() {
          resolve(true)
        })
        .error(function(error) {
          reject(error)
        });
      });
    },

    update: function(id, data) {
      return $q(function(resolve, reject) {
        $http.post(apiUrl + '/' + id, data)
        .success(function() {
          resolve(true)
        })
        .error(function(response) {
          reject(response)
        });
      });
    }


  };
});