Services.service('networkTypeService', function($http, $q) {
  var apiUrl = '/api/network-types';

  return {
    getAll: function() {
      return $q(function(resolve) {
        $http.get(apiUrl)
          .then(function(response) {
            resolve(response.data)
          });
      });
    },

    get: function(id) {
      return $q(function(resolve) {
        $http.get(apiUrl + '/' + id)
        .then(function(response) {
          resolve(response.data)
        });
      });
    },

    insert: function(data) {
      return $q(function(resolve, reject) {
        $http.post(apiUrl + '/insert', data)
        .success(function() {
          resolve(true)
        })
        .error(function(response) {
          reject(response)
        });
      });
    },

    update: function(id, data) {
      return $q(function(resolve, reject) {
        $http.post(apiUrl + '/' + id, data)
        .success(function() {
          resolve(true)
        })
        .error(function(response) {
          reject(response)
        });
      });
    }

    
  };
});