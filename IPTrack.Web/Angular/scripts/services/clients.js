Services.service('clientService', function($http, $q) {
  return {
    getAll: function(params) {
      return $q(function(resolve) {
        params = params || {};
        url = '/api/clients';

        $http({
          url: url, 
          params: {term: params.term},
          method: 'GET'
        })
          .then(function(response) {
            resolve(response.data)
          });
      });
    },

    get: function(id) {
      return $q(function(resolve) {
        $http.get('/api/clients/' + id)
          .then(function(response) {
            resolve(response.data.profile)
          });
      });
    },

    delete: function(id) {
      return $q(function(resolve) {
        $http({
          url: '/api/clients/' + id,
          method: 'DELETE',
          success: function(response) {
            resolve(response.data)
          }
        }).then(function() { resolve(true) } );
      });
    }
  };
});