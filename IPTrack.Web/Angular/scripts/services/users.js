Services.service('userService', function($http, $q) {
  var apiUrl = '/api/users';

  return {
    getAll: function() {
      return $q(function(resolve) {
        $http.get(apiUrl)
          .then(function(response) {
            resolve(response.data)
          });
      });
    },

    get: function(id) {
      return $q(function(resolve) {
        $http.get(apiUrl + '/' + id)
          .then(function(response) {
            resolve(response.data)
          });
      });
    },

    insert: function(data) {
      return $q(function(resolve, reject) {
        $http.post(apiUrl + '/insert', data)
        .success(function() {
          resolve(true)
        })
        .error(function(error) {
          reject(error)
        });
      });
    },

    update: function(id, data) {
      return $q(function(resolve, reject) {
        $http.post(apiUrl + '/' + id, data)
        .success(function() {
          resolve(true)
        })
        .error(function(error) {
          reject(error)
        });
      });
    },

    archive: function(id) {
      return $q(function(resolve, reject) {
        $http.post(apiUrl + '/archive/' + id)
        .success(function() {
          resolve(true)
        })
        .error(function(error) {
          reject(error)
        });
      });
    },

    getByUsernameOrEmail: function(usernameEmail) {
      return $q(function(resolve, reject) {
        $http.get(apiUrl + '/getByUsernameOrEmail/' + usernameEmail)
        .success(function(response) {
          resolve(response)
        })
        .error(function(error) {
          reject(error)
        });
      });
    }
    
  };
});