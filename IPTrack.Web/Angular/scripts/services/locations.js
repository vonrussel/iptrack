Services.service('locationService', function($http, $q) {
  var apiUrl = '/api/locations';

  return {
    getAll: function() {
      return $q(function(resolve) {
        $http.get(apiUrl)
        .then(function(response) {
          resolve(response.data)
        });
      });
    },

    getClientOf: function(clientId, params) {
      return $q(function(resolve) {
        params = params || {};
        $http({
          url: apiUrl,
          params: {
            clientId: clientId,
            term: params.term
          },
          method: 'GET'
        })
          .then(function(response) {
            resolve(response.data)
          });
      });
    }

    
  };
});