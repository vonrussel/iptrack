Services.service('iptrackerService', function($http, $q) {
  return {
    getAll: function(networkId) {
      return $q(function(resolve) {
        $http.get("/iptracker/getAllAssigned/" + networkId)
        .then(function(response) {
          resolve(response.data)
        })
      })
    },

    get: function(ipId) {
      return $q(function(resolve) {
        $http.get("/iptracker/get/" + ipId)
        .then(function(response) {
          resolve(response.data)
        })
      })
    },

    getByLocalAddress: function(localAddress) {
      return $q(function(resolve) {
        $http.get("/iptracker/get-by-local/" + localAddress)
        .then(function(response) {
          resolve(response.data)
        })
      })
    },

    assign: function(model) {
      return $q(function(resolve) {
        $http.post("/iptracker/assign", model)
        .then(function() {
          resolve(true)
        });
      });
    },

    deleteAssigned: function(id) {
      return $q(function(resolve) {
        $http({
          url: "/iptracker/delete/" + id,
          method: 'DELETE'
        })
          .then(function() {
            resolve(true)
          });
      });
    },

    update: function(id, model) {
      return $q(function(resolve) {
        $http.post("/iptracker/update/" + id, model)
        .then(function() {
          resolve(true)
        });
      });
    }

  };
});