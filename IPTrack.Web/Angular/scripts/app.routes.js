﻿
angular.module('app.routes', ['ngRoute'])

.config(function ($routeProvider, $locationProvider) {

    var viewsBasePath = '/angular/views/';

    $routeProvider.
        when('/', {
            title: 'Homepage',
            templateUrl: viewsBasePath + 'home/index.html',
            reloadOnSearch: false
        }).
        when('/about', {
            title: 'About page',
            templateUrl: viewsBasePath + 'home/about.html'
        }).
        when('/login', {
            title: 'Login page',
            templateUrl: viewsBasePath + 'home/login.html'
        }).

        // clients
        when('/clients', {
            title: 'Clients',
            templateUrl: viewsBasePath + 'clients/index.html'
        }).
        when('/clients/:id', {
            title: 'Clients',
            templateUrl: viewsBasePath + 'clients/form.html'
        }).

        // locations under client route
        when('/clients/:clientId/locations', {
            title: 'Client locations',
            templateUrl: viewsBasePath + 'locations/index.html'
        }).
        when('/clients/:clientId/locations/:locationId', {
            title: 'Client location',
            templateUrl: viewsBasePath + 'locations/form.html'
        }).

        // networks
        when('/networks', {
            title: 'Networks',
            templateUrl: viewsBasePath + 'networks/index.html'
        }).
        when('/networks/:id', {
            title: 'Networks',
            templateUrl: viewsBasePath + 'networks/form.html'
        }).

        // device types
        when('/device-types', {
            title: 'Device Types',
            templateUrl: viewsBasePath + 'device-types/index.html'
        }).
        when('/device-types/:id', {
            title: 'Device Types',
            templateUrl: viewsBasePath + 'device-types/form.html'
        }).

        // network types
        when('/network-types', {
            title: 'Network Types',
            templateUrl: viewsBasePath + 'network-types/index.html'
        }).
        when('/network-types/:id', {
            title: 'Network Types',
            templateUrl: viewsBasePath + 'network-types/form.html'
        }).

        // users
        when('/users', {
            title: 'Users',
            templateUrl: viewsBasePath + 'users/index.html'
        }).
        when('/users/:id', {
            title: 'Users',
            templateUrl: viewsBasePath + 'users/form.html'
        }).


        otherwise({
            title: 'Not found',
            templateUrl: viewsBasePath + '404.html'
        });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

});