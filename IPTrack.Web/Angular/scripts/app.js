﻿
var app = angular.module('app', 
  ['focus-if' , 'smart-table', 'app.routes', 'app.services',
  'ui.select', 'ngSanitize', 'ui.bootstrap'])

.config(function(uiSelectConfig) {
    //uiSelectConfig.theme = 'bootstrap';
  })

.run(['$rootScope', '$http', '$location', 'userService', function($root, $http, $location, userService) {
    // move this
    $root.logout = function() {
      $http.post('/api/accounts/logout').
      success(function() {
        $root.authenticated = false;
        $location.path('/login').replace();
      });
    };


    $root.$on('$routeChangeStart', function(next, current) { 
      if(! $root.authenticated) {
        $location.path('/login').replace();
      }
    });


    // root variables
    $root.isInRole = function(role) {
      if(! $root.authenticated) return;
      return _.contains($root.currentUser && $root.currentUser.profile.roles.split(","), role);
    };
    $root.isActiveRoute = function(path) {
      return $location.path() == path;
    };
    $root.$location = $location;
  }]);