app.controller('networkFormController', 
  ['$scope', '$route', '$http', '$location', 'clientService', 'locationService', 'networkService', 'networkTypeService', '$window',
  function($scope, $route, $http, $location, clientService, locationService, networkService, networkTypeService, $window) {

    var id = $route.current.params.id;
    var isInsert = id == 'new';

    // get from url state
    $scope.network = {};
    _search = $location.search();
    if(! _.isEmpty(_search)) {
      $scope.network.clientId = _search.clientid;
      $scope.network.locationId = _search.locationid;
    }

    $scope.clients = [];
    $scope.locations = [];
    $scope.networkTypes = [];
    $scope.clientsLoading = false;
    $scope.locationsLoading = false;
    $scope.networkTypesLoading = false;
    $scope.getClients = function(name) {
      $scope.clientsLoading = true;
      clientService.getAll()
      .then(function(response) {
        $scope.clientsLoading = false;
        $scope.clients = response
      });
    };
    $scope.getLocations = function(name) {
      if(! $scope.network.clientId) return;
      $scope.locationsLoading = true;
      locationService.getClientOf($scope.network.clientId)
      .then(function(response) {
        $scope.locationsLoading = false;
        $scope.locations = response
      });
    };
    $scope.getNetworkTypes = function(name) {
      $scope.networkTypesLoading = true;
      networkTypeService.getAll()
        .then(function(response) {
          $scope.networkTypesLoading = false;
          $scope.networkTypes = response
        });
    };
    $scope.clientIdChanged = function(clientId) {
      $scope.network.locationId = null;
      $scope.getLocations();
    };
    $scope.addUpdate = function(network) {
      $('.form-errors').html('');
      if(isInsert) {
        networkService.insert(network)
          .then(function(success) {
            if(! success) return;
            if(_.isEmpty(_search)) 
              $location.path('/networks').replace();
            else
              $window.history.back();
          }, function(response) {
            if(! response.errors.length) return;
            $('.form-errors').append(
              "<div class='alert alert-danger'>" +
                (_.map(response.errors, function(err) { return err.message })).join('<br />') +
              "</div>"
            )
          });
      } else if(id) { // is update
        networkService.update(id, network)
          .then(function(success) {
            if(success)
              $location.path('/networks').replace();
          }, function(response) {
            if(! response.errors.length) return;
            $('.form-errors').append(
              "<div class='alert alert-danger'>" +
                (_.map(response.errors, function(err) { return err.message })).join('<br />') +
              "</div>"
            )
          });
      }
    };

    if(!isInsert && id) {
      networkService.get(id)
      .then(function(data) {
        $scope.network = data.profile;
        // fix when clientId has initial value
        $scope.getLocations();
      });
    }

  }]
)

.controller('networkListingController', 
  ['$scope', '$http', 'networkService', function($scope, $http, networkService) {
    $scope.displayedNetworks = [];

    networkService.getAll()
    .then(function(data) {
      data = _.map(data, function(d) {
        return _.extend(d, {
          name: d.profile.name,
          cClass: d.profile.cClass
        })
      })
      $scope.networks = data;
    });

  }]
  )


.controller('networkEditModalController', 
  ['$scope', '$http', 'networkService', 'network', '$modalInstance', 'networkTypeService',
  function($scope, $http, networkService, network, $modalInstance, networkTypeService) {
    $scope.network = angular.copy(network);
    $scope.closeModal = function() {
      $modalInstance.dismiss();
    }
    $scope.update = function(model) {
      $('.form-errors').html('');
      networkService.update(model.id, model)
        .then(function() {
          $modalInstance.close(model);
        }, function(response) {
          if(! response.errors.length) return;
          $('.form-errors').append(
            "<div class='alert alert-danger'>" +
              (_.map(response.errors, function(err) { return err.message })).join('<br />') +
            "</div>"
          )
        });
    };

    $scope.networkTypesLoading = false;
    $scope.getNetworkTypes = function(name) {
      $scope.networkTypesLoading = true;
      networkTypeService.getAll()
        .then(function(response) {
          $scope.networkTypesLoading = false;
          $scope.networkTypes = response
        });
    };
  }]
  )