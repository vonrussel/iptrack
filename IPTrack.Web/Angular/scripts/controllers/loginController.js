app.controller('loginController', 
  ['$scope', '$http', '$window', '$rootScope', '$location', 'userService', 
  function($scope, $http, win, $root, $location, userService) {

    // redirect to home if logged
    if($root.authenticated) {
      redirect();
    }

    $scope.login = function(user) {
      $('.error-messages').addClass('hidden');
      $http.post('/api/accounts/auth', user)
      .success(function(response) {
        if(response/* && response.success*/) {
          $root.authenticated = true;
          userService.getByUsernameOrEmail(user.username)
          .then(function(response) {
            $root.currentUser = response;
          });
          redirect();
        }

      })
      .error(function() {
        $('.error-messages').removeClass('hidden');
      });
    };

    function redirect() {
      $location.path('/').replace();
    }
  }]);