app


.controller('locationFormController', 
  ['$scope', '$route', '$http', '$location', '$window', 
    function($scope, $route, $http, $location, $window) {

    var id = $route.current.params.locationId;
    var clientId = $route.current.params.clientId;
    var isInsert = id == 'new';
    $scope.location = {};
    angular.extend($scope.location, {clientId: clientId});

    if(!isInsert && id) {
      $http.get('/api/locations/' + id)
      .success(function(data) {
        $scope.location = data.profile;
      });
    }

    $scope.addUpdate = function(location) {
      if(isInsert) {
        $http.post('/api/locations/insert', $scope.location)
        .success(function() {
          if(($location.search() || {}).redirect)
            $window.history.back();
          else
            $location.path('/clients/' + clientId + '/locations').replace();
        });
      } else if(id) { // is update
        $http.post('/api/locations/' + id, $scope.location)
        .success(function() {
          $location.path('/clients/' + clientId + '/locations').replace();
        });
      }
    };

  }]
)

.controller('locationListingController', 
  ['$scope', '$route', '$http', 'clientService', function($scope, $route, $http, clientService) {
    $scope.clientId = $route.current.params.clientId;
    $scope.locations = [];
    $http({
      url: '/api/locations', 
      params: {clientId: $scope.clientId},
      method: 'GET'
    }).
    success(function(data) {
      $scope.locations = data;
    });

    clientService.get($scope.clientId)
      .then(function(data) {
        $scope.client = data;
      });
  }]
);