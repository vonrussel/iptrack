app


.controller('userFormController', 
  ['$scope', '$route', '$http', '$location', '$window', 'userService', function($scope, $route, $http, $location, $window, service) {

    var id = $route.current.params.id;
    var isInsert = id == 'new';
    $scope.model = {};
    $scope.isInsert = isInsert;

    if(!isInsert && id) {
      service.get(id)
        .then(function(data) {
          $scope.model = data.profile;
        });
    }

    // TODO maybe add service for roles?
    $scope.roles = ["admin", "staff"];

    $scope.addUpdate = function(model) {
      $('.form-errors').html('');
      if(isInsert) {
        service.insert($scope.model)
          .then(function() {
            if(($location.search() || {}).redirect)
              $window.history.back();
            else
              $location.path('/users').replace();
          }, function(response) {
            if(! response.errors.length) return;
            $('.form-errors').append(
              "<div class='alert alert-danger'>" +
                (_.map(response.errors, function(err) { return err.message })).join('<br />') +
              "</div>"
            )
          });
      } else if(id) { // is update
        service.update(id, $scope.model)
          .then(function() {
            $location.path('/users').replace();
          }, function(response) {
          if(! response.errors.length) return;
          $('.form-errors').append(
            "<div class='alert alert-danger'>" +
              (_.map(response.errors, function(err) { return err.message })).join('<br />') +
            "</div>"
          )
        });
      }
    };

  }]
)

.controller('userListingController', 
  ['$scope', '$route', '$http' , 'userService', function($scope, $route, $http, service) {
    $scope.displayedUsers = [];

    service.getAll()
      .then(function(data) {
        $scope.users = data;
      });

    $scope.archive = function(id) {
      if(! confirm("Continue to delete user?")) return;
      service.archive(id)
        .then(function() {
          $scope.users = _.filter($scope.users, function(row) {
            return row.profile.id != id;
          })
        })
    };


  }]
);