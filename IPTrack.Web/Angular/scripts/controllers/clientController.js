app

.controller('clientFormController', 
  ['$scope', '$route', '$http', '$location', '$window', function($scope, $route, $http, $location, $window) {

    var id = $route.current.params.id;
    var isInsert = id == 'new';
    $scope.client = {};

    if(!isInsert && id) {
      $http.get('/api/clients/' + id)
      .success(function(data) {
        $scope.client = data.profile;
      });
    }

    $scope.addUpdate = function(client) {
      $('.form-errors').html('');
      if(isInsert) {
        $http.post('/api/clients/insert', $scope.client)
          .success(function() {
            if(($location.search() || {}).redirect)
              $window.history.back();
            else
              $location.path('/clients').replace();
          })
          .error(function(response) {
            $('.form-errors').append(
              "<div class='alert alert-danger'>" +
                (_.map(response.errors, function(err) { return err.message })).join('<br />') +
              "</div>"
            )
          });
      } else if(id) { // is update
        $http.post('/api/clients/' + id, $scope.client)
          .success(function() {
            $location.path('/clients').replace();
          })
          .error(function(response) {
            $('.form-errors').append(
              "<div class='alert alert-danger'>" +
                (_.map(response.errors, function(err) { return err.message })).join('<br />') +
              "</div>"
            )
          });
      }
    };

}]
)

.controller('clientListingController', 
  ['$scope', '$http', 'clientService' , function($scope, $http, clientService) {
    $scope.displayedClients = [];

    $http.get('/api/clients')
    .success(function(data) {
      $scope.clients = data;
    });

    $scope.delete = function(id) {
      if(confirm("Confirm delete?")) {
        clientService.delete(id);
        $scope.clients = $.grep($scope.clients, function(n) {
          return n.profile.id != id;
        });
      }
    };

  }]
  );