app.controller('ipAddressController', 
      ['$scope', '$route', '$http', '$location', 'clientService', 'locationService', 'networkService', '$modal',
      function($scope, $route, $http, $location, clientService, locationService, networkService, $modal) {

    var id = $route.current.params.id;
    var isInsert = id == 'new';


    $scope.clients = [];
    $scope.locations = [];
    $scope.networks = [];
    $scope.clientsLoading = false;
    $scope.locationsLoading = false;
    $scope.networksLoading = false;
    //$scope.selectedLocation = '';

    // get state from url
    _search = $location.search();
    $scope.model = {};
    if(! _.isEmpty(_search)) {
      $scope.model = {
        selectedClient: _search.clientid,
        selectedLocation: _search.locationid,
        selectedNetwork: _search.networkid
      };
    }

    $scope.getClients = function(name) {
      $scope.clientsLoading = true;
      clientService.getAll({term : name})
        .then(function(response) {
          $scope.clientsLoading = false;
          $scope.clients = response
        });
    };
    $scope.getLocations = function(term) {
      if(! $scope.model.selectedClient) {
        $scope.locations = [];
        return;
      }
      $scope.locationsLoading = true;
      $scope.locations = [];
      $scope.networks = [];
      locationService.getClientOf($scope.model.selectedClient, {term: term})
        .then(function(response) {
          // change also values of location and network
          $scope.locationsLoading = false;
          $scope.locations = response;
        });
    };
    $scope.getNetworks = function(term) {
      if(! $scope.model.selectedLocation) {
        $scope.networks = [];
        return;
      }
      $scope.networksLoading = true;
      networkService.getByLocation($scope.model.selectedLocation, {term: term})
        .then(function(response) {
          $scope.networksLoading = false;
          $scope.networks = response;
       });
    };
    previousLocation = '';
    $scope.onLocationSelect = function(locationId) {
      if(previousLocation != locationId) {
        
        // add state to url
        search = _.extend($location.search() || {}, {locationid: locationId})
        $location.path('/').search(search).replace();
        $scope.model.selectedNetwork = null;

        $scope.getNetworks();
        previousLocation = locationId;
      }
    };
    $scope.clientIdChanged = function(clientId) {
      // reset
      previousLocation = '';
      $scope.getLocations();
      $scope.network = {};

      $scope.model.selectedLocation = null;
      $scope.model.selectedNetwork = null;

      // add state to url
      $location.path('/').search({clientid: clientId}).replace();

      clientService.get(clientId)
        .then(function(client) {
          $scope.client = client;
        });
    };
    $scope.onNetworkChanged = function(networkId) {
      // add state to url
      search = _.extend($location.search() || {}, {networkid: networkId})
      $location.path('/').search(search).replace();
    };
    $scope.addUpdate = function(network) {
      if(isInsert) {
        networkService.insert(network)
        .then(function(success) {
          if(success)
            $location.path('/networks').replace();
        });
      } else if(id) { // is update
        networkService.update(id, network)
        .then(function(success) {
          if(success)
            $location.path('/networks').replace();
        });
      }
    };

    // popup network edit form modal
    $scope.editNetwork = function() {
      var modal = $modal.open({
        controller: 'networkEditModalController',
        resolve: {
          network: function() {
            return $scope.network;
          }
        },
        size: 'lg',
        templateUrl: '/angular/views/networks/modal-form.html'
      });

      modal.result.then(function(network) {
        $scope.network = network;
      });
    };

    $scope.$watch('model.selectedNetwork', function(networkId) {
      refreshNetworkData(networkId);
    });

    function refreshNetworkData(networkId) {
      if(! networkId) {
        return $scope.network = {}
      }
      networkService.get(networkId)
        .then(function(response) {
          $scope.network = response.profile;
        });
    }

  }]
);