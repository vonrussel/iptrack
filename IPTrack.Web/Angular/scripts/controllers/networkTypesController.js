app


.controller('networkTypeFormController', 
  ['$scope', '$route', '$http', '$location', '$window', 'networkTypeService', function($scope, $route, $http, $location, $window, networkTypeService) {

    var id = $route.current.params.id;
    var isInsert = id == 'new';
    $scope.model = {};

    if(!isInsert && id) {
      networkTypeService.get(id)
        .then(function(data) {
          $scope.model = data.profile;
        });
    }

    $scope.addUpdate = function(model) {
      $('.form-errors').html('');
      if(isInsert) {
        networkTypeService.insert($scope.model)
          .then(function() {
            if(($location.search() || {}).redirect)
              $window.history.back();
            else
              $location.path('/network-types').replace();
          }, function(response) {
            $('.form-errors').append(
                "<div class='alert alert-danger'>" +
                  (_.map(response.errors, function(err) { return err.message })).join('<br />') +
                "</div>"
              )
          });
        } else if(id) { // is update
          networkTypeService.update(id, $scope.model)
            .then(function() {
              $location.path('/network-types').replace();
            }, function(response) {
              $('.form-errors').append(
                "<div class='alert alert-danger'>" +
                  (_.map(response.errors, function(err) { return err.message })).join('<br />') +
                "</div>"
              )
            });
        }
    };

  }]
)

.controller('networkTypeListingController', 
  ['$scope', '$route', '$http' , 'networkTypeService', function($scope, $route, $http, networkTypeService) {
    $scope.displayedNetworkTypes = [];

    networkTypeService.getAll()
      .then(function(data) {
        $scope.networkTypes = data;
      });
  }]
);