app


.controller('deviceTypeFormController', 
  ['$scope', '$route', '$http', '$location', '$window', 'deviceTypeService', function($scope, $route, $http, $location, $window, deviceTypeService) {

    var id = $route.current.params.id;
    var isInsert = id == 'new';
    $scope.model = {};

    if(!isInsert && id) {
      deviceTypeService.get(id)
        .then(function(data) {
          $scope.model = data.profile;
        });
    }

    $scope.addUpdate = function(model) {
      $('.form-errors').html('');
      if(isInsert) {
        deviceTypeService.insert($scope.model)
          .then(function() {
            if(($location.search() || {}).redirect)
              $window.history.back();
            else
              $location.path('/device-types').replace();
          }, function(response) {
            $('.form-errors').append(
              "<div class='alert alert-danger'>" +
                (_.map(response.errors, function(err) { return err.message })).join('<br />') +
              "</div>"
            )
          });
        } else if(id) { // is update
          deviceTypeService.update(id, $scope.model)
            .then(function() {
              $location.path('/device-types').replace();
            }, function(response) {
              $('.form-errors').append(
                "<div class='alert alert-danger'>" +
                  (_.map(response.errors, function(err) { return err.message })).join('<br />') +
                "</div>"
              )
            });
        }
    };

  }]
)

.controller('deviceTypeListingController', 
  ['$scope', '$route', '$http' , 'deviceTypeService', function($scope, $route, $http, deviceTypeService) {
    $scope.displayedDeviceTypes = [];

    deviceTypeService.getAll()
      .then(function(data) {
        $scope.deviceTypes = data;
      });
  }]
);