﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Web.Validators.DeviceTypes
{
    using FluentValidation;

    public class Model : DeviceTypeTable
    {

    }

    public class Validator : AbstractValidator<Model>
    {
        public Validator()
        {
            RuleSet("manager", () =>
            {
                RuleFor(m => m.name).NotEmpty();
                Custom(m =>
                {
                    string cleanName = m.name.Trim().ToUpper();

                    // if update
                    if (m.id > 0)
                    {
                        var existing = IPTrack.Models.DeviceType.Get(c => 
                            c.name.Trim().ToUpper() == cleanName &&
                            c.id != m.id
                        );
                        if (existing.Count() > 0)
                        {
                            return new FluentValidation.Results.ValidationFailure("name", "Device type name should be unique");
                        }
                    }
                    else
                    {

                        var existing = IPTrack.Models.DeviceType.Get(c => c.name.Trim().ToUpper() == cleanName);
                        if (existing.Count() > 0)
                        {
                            return new FluentValidation.Results.ValidationFailure("name", "Device type name should be unique");
                        }
                    }

                    return null;
                });
            });
        }
    }

}
