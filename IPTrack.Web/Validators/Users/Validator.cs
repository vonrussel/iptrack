﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Web.Validators.Users
{
    using FluentValidation;

    public class Model : UserTable
    {
        public string repeat_password { get; set; }
    }

    public class Validator : AbstractValidator<Model>
    {
        public Validator()
        {
            RuleSet("insert", () =>
            {
                RuleFor(m => m.username).NotNull();
                RuleFor(m => m.username).Matches(@"^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$")
                    .WithMessage("Username should be atleast 8 to 20 characters");
                RuleFor(m => m.roles).NotNull();
                RuleFor(m => m.email).NotNull();
                RuleFor(m => m.email).EmailAddress();
                RuleFor(m => m.password).NotNull();
                RuleFor(m => m.repeat_password).NotNull();
                
                Custom(m =>
                {
                    // repeat password
                    if (m.password != m.repeat_password)
                    {
                        return new FluentValidation.Results.ValidationFailure("password", "Password does not match");
                    }

                    // username or email exists
                    if (new IPTrack.Models.User(username: m.username).profile != null)
                    {
                        return new FluentValidation.Results.ValidationFailure("username", "Username already in use");
                    }
                    if (new IPTrack.Models.User(username: m.email).profile != null)
                    {
                        return new FluentValidation.Results.ValidationFailure("email", "Email address already in use");
                    }

                    return null;
                });
            });

            RuleSet("update", () =>
            {
                RuleFor(m => m.username).NotNull();
                RuleFor(m => m.username).Matches(@"^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$")
                    .WithMessage("Username should be atleast 8 to 20 characters");
                RuleFor(m => m.roles).NotNull();
                RuleFor(m => m.email).NotNull();
                RuleFor(m => m.email).EmailAddress();

                Custom(m =>
                {
                    // username or email exists
                    var user = new IPTrack.Models.User(username: m.username);
                    if (user.profile != null && user.profile.id != m.id)
                    {
                        return new FluentValidation.Results.ValidationFailure("username", "Username already in use");
                    }
                    user = new IPTrack.Models.User(username: m.email);
                    if (user.profile != null && user.profile.id != m.id)
                    {
                        return new FluentValidation.Results.ValidationFailure("email", "Email address already in use");
                    }

                    return null;
                });
            });

        }
    }

}
