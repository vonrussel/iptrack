﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Web.Validators.Networks
{
    using FluentValidation; 

    public class Model : NetworkTable
    {

    }

    public class Validator : AbstractValidator<Model>
    {
        public Validator()
        {
            RuleSet("manager", () =>
            {
                RuleFor(m => m.cClass).NotNull();
                RuleFor(m => m.cClass).Matches(Validators.Global.REGEX_CCLASS).WithMessage("C-Class is not in valid format");
                RuleFor(m => m.dns1).Matches(Validators.Global.REGEX_IP).WithMessage("DNS1 is not in valid format").When(m => !String.IsNullOrEmpty(m.dns1));
                RuleFor(m => m.dns2).Matches(Validators.Global.REGEX_IP).WithMessage("DNS2 is not in valid format").When(m => !String.IsNullOrEmpty(m.dns2));
                RuleFor(m => m.dns3).Matches(Validators.Global.REGEX_IP).WithMessage("DNS3 is not in valid format").When(m => !String.IsNullOrEmpty(m.dns3));
                RuleFor(m => m.dns4).Matches(Validators.Global.REGEX_IP).WithMessage("DNS4 is not in valid format").When(m => !String.IsNullOrEmpty(m.dns4));
                RuleFor(m => m.subnet).Matches(Validators.Global.REGEX_IP).WithMessage("Subnet Mask is not in valid format").When(m => !String.IsNullOrEmpty(m.subnet));
                RuleFor(m => m.gateway).Matches(Validators.Global.REGEX_IP).WithMessage("Gateway is not in valid format").When(m => !String.IsNullOrEmpty(m.gateway));
                RuleFor(m => m.rangeFrom).GreaterThanOrEqualTo(0);
                RuleFor(m => m.rangeTo).LessThanOrEqualTo(255);
                RuleFor(m => m.dhcpFrom).GreaterThanOrEqualTo(0);
                RuleFor(m => m.dhcpTo).LessThanOrEqualTo(255);

                // for IP addresses
                Custom(m =>
                {
                    if (!Validators.Global.IsValidIP(m.cClass, 3))
                        return new FluentValidation.Results.ValidationFailure("cClass", "C-Class must range 0 to 255");

                    if (!Validators.Global.IsValidIP(m.dns1))
                        return new FluentValidation.Results.ValidationFailure("dns1", "DNS 1 must range 0 to 255");

                    if (!Validators.Global.IsValidIP(m.dns2))
                        return new FluentValidation.Results.ValidationFailure("dns2", "DNS 2 must range 0 to 255");

                    if (!Validators.Global.IsValidIP(m.dns3))
                        return new FluentValidation.Results.ValidationFailure("dns3", "DNS 3 must range 0 to 255");

                    if (!Validators.Global.IsValidIP(m.dns4))
                        return new FluentValidation.Results.ValidationFailure("dns4", "DNS 4 must range 0 to 255");

                    if (!Validators.Global.IsValidIP(m.subnet))
                        return new FluentValidation.Results.ValidationFailure("subnet", "Subnet Mask must range 0 to 255");

                    if (!Validators.Global.IsValidIP(m.gateway))
                        return new FluentValidation.Results.ValidationFailure("gateway", "Gateway must range 0 to 255");


                    return null;
                });

                // ranges of dhcp
                Custom(m =>
                {
                    if (m.dhcpFrom != 0 && m.dhcpTo != 0)
                    {
                        if (!(m.dhcpFrom >= m.rangeFrom && m.dhcpTo <= m.rangeTo))
                        {
                            return new FluentValidation.Results.ValidationFailure("dhcpFrom", "DHCP Ranges should be between IP Ranges");
                        }
                    }
                    return null;
                });

                RuleFor(m => m.clientId).NotNull();
                RuleFor(m => m.DHCPServer).NotNull();
                RuleFor(m => m.dhcpFrom).NotNull();
                RuleFor(m => m.dhcpTo).NotNull();
                RuleFor(m => m.rangeFrom).NotNull();
                RuleFor(m => m.rangeTo).NotNull();
                RuleFor(m => m.locationId).NotNull();
                RuleFor(m => m.networkTypeId).NotNull();
            });
     
        }
    }

}
