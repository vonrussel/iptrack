﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Web.Validators
{
    public class Global
    {
        public static string REGEX_IP = @"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$";
        public static string REGEX_CCLASS = @"^\d{1,3}\.\d{1,3}\.\d{1,3}$";


        public static bool IsValidIP(string ipString, int partsLength = 4) {
            string[] ip = (ipString ?? "").Split('.');
            for (var i = 1; i <= partsLength; i++)
            {
                uint part = 0;
                try
                {
                    part = Convert.ToUInt32(ip[i]);
                }
                catch (Exception) { }

                if (part < 0 || part > 255)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
