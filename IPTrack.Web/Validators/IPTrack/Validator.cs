﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPTrack.Web.Validators.IPTracker
{
    using FluentValidation;

    public class Model : IPAddressTable
    {

    }

    public class Validator : AbstractValidator<Model>
    {
        public Validator()
        {
            RuleSet("manager", () =>
            {
                RuleFor(m => m.IpAddress).NotNull();
                RuleFor(m => m.IpAddress).Matches(Validators.Global.REGEX_IP).WithMessage("IP Address is not in valid format");

                // for IP addresses
                Custom(m =>
                {
                    if (!Validators.Global.IsValidIP(m.IpAddress, 4))
                        return new FluentValidation.Results.ValidationFailure("IpAddress", "IP Address must range 0 to 255");

                    return null;
                });

                RuleFor(m => m.information).NotNull();
                RuleFor(m => m.networkId).NotNull();
                RuleFor(m => m.deviceTypeId).NotNull();
            });

        }
    }

}
