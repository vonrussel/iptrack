﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using FluentValidation;


namespace IPTrack.Web.API
{
    [Authorize]
    [RoutePrefix("iptracker")]
    public class IPTrackerController : BaseApiController
    {

        [Route("assign")]
        public HttpResponseMessage Assign([FromBody] Validators.IPTracker.Model _params)
        {

            var user = new IPTrack.Models.User(username: User.Identity.Name);
            var validator = new Validators.IPTracker.Validator();
            var result = validator.Validate(_params, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            try
            {
                IPTrack.Models.IPTracker.AssignToIp(
                    ipAddress: (String)_params.IpAddress,
                    networkId: (int)_params.networkId,
                    comment: (String)_params.comment,
                    information: (String)_params.information,
                    deviceType: (int)_params.deviceTypeId,
                    currentUser: user
                );
            }
            catch (Exception)
            {
                
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("getAllAssigned/{networkId:int}")]
        [HttpGet]
        public IEnumerable<IPAddressTable> GetAllAssigned(int networkId)
        {
            return IPTrack.Models.IPTracker.GetAllAssigned().Where(n => n.networkId == networkId);
        }

        [Route("get/{ipId:int}")]
        [HttpGet]
        public IPAddressTable GetIP(int ipId)
        {
            return IPTrack.Models.IPTracker.GetIP(ipId);
        }

        [Route("get-by-local/{local:int}")]
        [HttpGet]
        public IPAddressTable GetByLocal(int local)
        {
            return IPTrack.Models.IPTracker.GetByLocal(local);
        }

        [Route("update/{ipId:int}")]
        public HttpResponseMessage Update(int ipId, [FromBody] Validators.IPTracker.Model _params)
        {

            var user = new IPTrack.Models.User(username: User.Identity.Name);
            var validator = new Validators.IPTracker.Validator();
            var result = validator.Validate(_params, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            try
            {
                IPTrack.Models.IPTracker.Update(
                    id: ipId,
                    data: new IPAddressTable() {
                        comment = (String)_params.comment,
                        information = (String)_params.information
                    },
                    currentUser: user
                );
            }
            catch (Exception)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("delete/{ipId:int}")]
        public IHttpActionResult DeleteAssigned(int ipId)
        {
            if ( ipId == null)
            {
                return InternalServerError(new ArgumentException("Parameters not valid"));
            }

            var user = new IPTrack.Models.User(username: User.Identity.Name);

            try
            {
                IPTrack.Models.IPTracker.DeleteAssigned(
                    id: ipId,
                    currentUser: user
                );
            }
            catch (Exception)
            {
                return InternalServerError(new ArgumentException("Error occured while removing assigned to IP"));
            }

            return Ok();
        }

        [HttpGet]
        [Route("export-to-csv/{id:int}")]
        public HttpResponseMessage ExportToCSV(int id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string path = AppDomain.CurrentDomain.BaseDirectory + @"Exports\";

            string fileName = Reports.ExportIPAddressToCSV(id, path);

            response.Content = new ByteArrayContent(File.ReadAllBytes(path + fileName));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName
            };
            return response;
        }





    }
}
