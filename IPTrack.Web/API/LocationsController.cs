﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IPTrack.Web.API
{

    [Authorize]
    [RoutePrefix("api/locations")]
    public class LocationsController : ApiController
    {

        public IHttpActionResult Insert([FromBody] dynamic location)
        {
            if (location == null)
            {
                return InternalServerError(new ArgumentException("Location parameter not defined"));
            }

            new IPTrack.Models.Location(new LocationTable()
            {
                name = (String)location.name,
                clientId = (int)location.clientId
            }).Save(User.Identity.Name);

            return Ok();
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<IPTrack.Models.Location> GetClientOf(int clientId, string term = "")
        {
            var ret = IPTrack.Models.Location.GetAll().Where(l => l.profile.clientId == clientId);
            if (!String.IsNullOrEmpty(term))
            {
                ret = ret.Where(l => l.profile.name.Contains(term));
            }
            return ret;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<IPTrack.Models.Location> GetAll()
        {
            return IPTrack.Models.Location.GetAll();
        }

        [HttpGet]
        [Route("{id:int}")]
        public IPTrack.Models.Location GetLocation(int id)
        {
            return new IPTrack.Models.Location(id);
        }

        [HttpPost]
        [Route("{id:int}")]
        public IHttpActionResult Update(int id, [FromBody] dynamic location)
        {
            if (location == null)
            {
                return InternalServerError(new ArgumentException("Client parameter not defined"));
            }

            var toSave = new IPTrack.Models.Location(id);
            toSave.profile.name = (String)location.name;
            toSave.profile.address = (String)location.address;
            toSave.profile.locationPh1 = (String)location.locationPh1;
            toSave.profile.locationPh2 = (String)location.locationPh2;
            toSave.profile.contactName = (String)location.contactName;
            toSave.Save(User.Identity.Name);

            return Ok();
        }


    }
}
