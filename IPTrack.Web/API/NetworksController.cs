﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FluentValidation;
using System.Web.Script.Serialization;

namespace IPTrack.Web.API
{

    [Authorize]
    [RoutePrefix("api/networks")]
    public class NetworksController : BaseApiController
    {

        public HttpResponseMessage Insert([FromBody] Validators.Networks.Model network)
        {
            var validator = new Validators.Networks.Validator();
            var result = validator.Validate(network, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            new IPTrack.Models.Network(NetworkTable.FromDynamic((dynamic)network)).Save(User.Identity.Name);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<IPTrack.Models.Network> Index()
        {
            return IPTrack.Models.Network.GetAll();
        }

        [HttpGet]
        [Route("GetByLocation/{locationId:int}")]
        public IEnumerable<IPTrack.Models.Network> GetByLocation(int locationId, string term = "")
        {
            var ret = IPTrack.Models.Network.GetAll()
                .Where(l => l.profile.locationId == locationId);

            if (!String.IsNullOrEmpty(term))
            {
                ret = ret.Where(n => n.profile.cClass.Contains(term));
            }
            return ret;
        }


        [HttpGet]
        [Route("{id:int}")]
        public IPTrack.Models.Network GetNetwork(int id)
        {
            return new IPTrack.Models.Network(id);
        }

        [HttpPost]
        [Route("{id:int}")]
        public HttpResponseMessage Update(int id, [FromBody] Validators.Networks.Model network)
        {
            var validator = new Validators.Networks.Validator();
            var result = validator.Validate(network, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            var toSave = new IPTrack.Models.Network(id);
            toSave.profile = NetworkTable.FromDynamic(network);
            toSave.Save(User.Identity.Name);

            return Request.CreateResponse(HttpStatusCode.OK);
        }


    }
}
