﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FluentValidation;

namespace IPTrack.Web.API
{

    [Authorize]
    [RoutePrefix("api/device-types")]
    public class NetworkTypeController : BaseApiController
    {
        [Route("insert")]
        public HttpResponseMessage Insert([FromBody] Validators.DeviceTypes.Model type)
        {
            var validator = new Validators.DeviceTypes.Validator();
            var result = validator.Validate(type, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            new IPTrack.Models.DeviceType(DeviceTypeTable.FromDynamic(type)).Save(User.Identity.Name);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<IPTrack.Models.DeviceType> Index()
        {
            return IPTrack.Models.DeviceType.GetAll();
        }

        [HttpGet]
        [Route("{id:int}")]
        public IPTrack.Models.DeviceType GetDeviceType(int id)
        {
            return new IPTrack.Models.DeviceType(id);
        }

        [HttpPost]
        [Route("{id:int}")]
        public HttpResponseMessage Update(int id, [FromBody] Validators.DeviceTypes.Model type)
        {
            var validator = new Validators.DeviceTypes.Validator();
            var result = validator.Validate(type, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            var toSave = new IPTrack.Models.DeviceType(id);
            toSave.profile = DeviceTypeTable.FromDynamic(type);
            toSave.Save(User.Identity.Name);
            return Request.CreateResponse(HttpStatusCode.OK);
        }


    }
}
