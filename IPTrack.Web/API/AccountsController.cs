﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace IPTrack.Web.API
{
    public class AccountsController : ApiController
    {

        public IHttpActionResult Auth([FromBody] dynamic user)
        {
            if (user == null)
            {
                return InternalServerError(new ArgumentException("User parameter not defined"));
            }

            string usernameOrEmail = (String)user.username;
            string password = (String)user.password;

            if (String.IsNullOrEmpty(usernameOrEmail) || String.IsNullOrEmpty(password))
            {
                return InternalServerError(new ArgumentException("Parameter not valid"));
            }

            if (!IPTrack.Models.User.Auth(usernameOrEmail, password))
            {
                return InternalServerError(new Exception("Not authorized"));
            }

            var _user = new IPTrack.Models.User(username: usernameOrEmail, email: usernameOrEmail);
            FormsAuthentication.SetAuthCookie(_user.profile.username, false);
            return Ok(new { });
        }

        public IHttpActionResult Logout()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return NotFound();
            }

            FormsAuthentication.SignOut();
            return Ok();
        }

    }
}
