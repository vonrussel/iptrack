﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FluentValidation;

namespace IPTrack.Web.API
{

    [Authorize]
    [RoutePrefix("api/clients")]
    public class ClientsController : BaseApiController
    {

        public HttpResponseMessage Insert([FromBody] Validators.Clients.Model client)
        {
            var validator = new Validators.Clients.Validator();
            var result = validator.Validate(client, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            new IPTrack.Models.Client(ClientTable.FromDynamic((dynamic)client)).Save(User.Identity.Name);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // listing
        [HttpGet]
        [Route("")]
        public IEnumerable<IPTrack.Models.Client> Index(string term = "")
        {
            if (! String.IsNullOrEmpty(term))
            {
                return IPTrack.Models.Client.GetAll().Where(c => c.profile.name.Contains(term));
            }
            return IPTrack.Models.Client.GetAll();
        }

        [HttpGet]
        [Route("{id:int}")]
        public IPTrack.Models.Client Index(int id)
        {
            return new IPTrack.Models.Client(id);
        }

        [HttpPost]
        [Route("{id:int}")]
        public HttpResponseMessage Update(int id, [FromBody] Validators.Clients.Model client)
        {
            var validator = new Validators.Clients.Validator();
            var result = validator.Validate(client, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            var toSave = new IPTrack.Models.Client(id);
            toSave.profile = ClientTable.FromDynamic(client);
            toSave.Save(User.Identity.Name);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        [Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            var user = new IPTrack.Models.User(username: User.Identity.Name); // cyrrent user for archive info
            IPTrack.Models.Client.Delete(id, user);
            return Ok();
        }


    }
}
