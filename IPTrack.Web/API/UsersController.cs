﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FluentValidation;
using System.Web.Script.Serialization;

namespace IPTrack.Web.API
{

    [Authorize(Roles = "admin")]
    [RoutePrefix("api/users")]
    public class UsersController : BaseApiController
    {

        public HttpResponseMessage Insert([FromBody] Validators.Users.Model user)
        {
            var validator = new Validators.Users.Validator();
            var result = validator.Validate(user, ruleSet: "insert");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            new IPTrack.Models.User(UserTable.FromDynamic((dynamic)user)).Save(User.Identity.Name);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<IPTrack.Models.User> Index()
        {
            return IPTrack.Models.User.GetAll().Select(u => {
                // do not send password to client
                u.profile.password = null;
                u.profile.password_salt = null;
                return u;
            });
        }

        [HttpGet]
        [Route("{id:int}")]
        public IPTrack.Models.User Get(int id)
        {
            var user = new IPTrack.Models.User(id);
            // do not send password to client
            user.profile.password = null;
            user.profile.password_salt = null;
            return user;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("getByUsernameOrEmail/{usernameEmail}")]
        public IPTrack.Models.User GetByUsernameOrEmail(string usernameEmail)
        {
            var user = new IPTrack.Models.User(username: usernameEmail, email: usernameEmail);
            // do not send password to client
            user.profile.password = null;
            user.profile.password_salt = null;
            return user;
        }

        [HttpPost]
        [Route("{id:int}")]
        public HttpResponseMessage Update(int id, [FromBody] Validators.Users.Model user)
        {
            var validator = new Validators.Users.Validator();
            var result = validator.Validate(user, ruleSet: "update");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            var toSave = new IPTrack.Models.User(id);
            toSave.profile = UserTable.FromDynamic(user);
            toSave.Save(User.Identity.Name);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("archive/{id:int}")]
        public HttpResponseMessage Archive(int id)
        {
            var currentUser = new IPTrack.Models.User(username: User.Identity.Name); // cyrrent user for archive info
            new IPTrack.Models.User(id).Archive(currentUser);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        

    }
}
