﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FluentValidation;

namespace IPTrack.Web.API
{

    [Authorize]
    [RoutePrefix("api/network-types")]
    public class DeviceTypeController : BaseApiController
    {
        [Route("insert")]
        public HttpResponseMessage Insert([FromBody] Validators.NetworkTypes.Model type)
        {
            var validator = new Validators.NetworkTypes.Validator();
            var result = validator.Validate(type, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            new IPTrack.Models.NetworkType(NetworkTypeTable.FromDynamic(type)).Save(User.Identity.Name);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<IPTrack.Models.NetworkType> Index()
        {
            return IPTrack.Models.NetworkType.GetAll();
        }

        [HttpGet]
        [Route("{id:int}")]
        public IPTrack.Models.NetworkType GetNetworkType(int id)
        {
            return new IPTrack.Models.NetworkType(id);
        }

        [HttpPost]
        [Route("{id:int}")]
        public HttpResponseMessage Update(int id, [FromBody] Validators.NetworkTypes.Model type)
        {
            var validator = new Validators.NetworkTypes.Validator();
            var result = validator.Validate(type, ruleSet: "manager");
            if (!result.IsValid)
            {
                return HandleError(result);
            }

            var toSave = new IPTrack.Models.NetworkType(id);
            toSave.profile = NetworkTypeTable.FromDynamic(type);
            toSave.Save(User.Identity.Name);
            return Request.CreateResponse(HttpStatusCode.OK);
        }


    }
}
