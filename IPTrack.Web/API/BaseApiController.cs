﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using FluentValidation;

namespace IPTrack.Web.API
{
    public class BaseApiController : ApiController
    {

        public HttpResponseMessage HandleError(FluentValidation.Results.ValidationResult result)
        {
            if (!result.IsValid)
            {
                var errors = new
                {
                    errors = result.Errors.Select(e =>
                    {
                        return new
                        {
                            field = e.PropertyName,
                            message = e.ErrorMessage
                        };
                    })
                };
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                var jsonString = new JavaScriptSerializer().Serialize(errors);
                response.Content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
